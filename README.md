# sylva-toolbox

The provided Dockerfiles build a lightweight Alpine Linux-based Docker image (basic and extended) with various Kubernetes-related tools. It sets up the environment with specified tool versions, installs dependencies, downloads tools, and verifies their versions. The resulting image includes the tools and relevant environment information in a version file. The CMD instruction creates a compressed archive of the tools and version information when the container is running.

In the Sylva project, the Renovate bot tool is integrated into GitLab CI to assist us in maintaining the images in an easier way.

The sylva toolbox containers two kind of images basic or extented tools. 

## Basic container includes the following tools :

1. **sylvactl:sylva-elements (Sylva's tool)** 
    The sylvactl client is designed to facilitate operations on the Sylva stack and currently supports the "watch" command. This command builds a dependency tree of flux resources and focuses on those currently in progress, hiding periodic reconciliations of ready resources or those with unmet dependencies. Future plans include setting up and configuring the sylva-units HelmRelease, providing additional features to simplify stack operations, improving flux convergence speed, measuring progress, setting timeouts, and using a standard client to promptly handle API connectivity issues. The implementation is considered a first functional version, with anticipated refactoring and the need to define an appropriate interface for tree objects, drawing on the flux2/cmd/flux/object as much as possible.
    for more details: https://gitlab.com/sylva-projects/sylva-elements/sylvactl

1. **kind (Kubernetes IN Docker)**:
   - `kind` is a tool for running local Kubernetes clusters using Docker container "nodes." It allows you to easily create, manage, and deploy Kubernetes clusters for development and testing purposes. Kind is lightweight and efficient, making it a popular choice for local Kubernetes environments.

1. **kubectl (Kubernetes Control)**
   - `kubectl` is the official command-line tool for interacting with Kubernetes clusters. It allows users to perform various operations, such as deploying applications, inspecting cluster resources, managing configurations, and troubleshooting issues within a Kubernetes cluster.

1. **kustomize (Kubernetes Native Configuration Management)**
   - `kustomize` is a tool for customizing Kubernetes manifests. It provides a declarative way to define and override configurations without directly modifying YAML files. With `kustomize`, users can manage configurations for different environments, share common configurations, and make updates to Kubernetes manifests in a more structured manner.

1. **Helm (Kubernetes Package Manager)**
   - `Helm` is a package manager for Kubernetes applications. It allows you to define, install, and upgrade even the most complex Kubernetes applications using pre-configured packages called charts. Helm simplifies the deployment and management of Kubernetes applications by providing a templating system and a centralized repository for sharing charts.

1. **Flux (GitOps Continuous Delivery Tool)**
   - `Flux` is a GitOps tool designed for continuous delivery in Kubernetes. It helps automate the deployment and synchronization of applications based on changes in a Git repository. Flux uses the Git repository as the source of truth for the desired cluster state, making it easier to manage and version control deployments.

1. **Yq (YAML Processor and Query Tool)**
   - `Yq` is a lightweight and flexible command-line tool for processing and querying YAML files. It allows users to perform various operations on YAML data, such as filtering, updating, and extracting information. Yq is particularly useful in the context of Kubernetes for working with YAML manifests and configuration files.

1. **Clusterctl (Cluster API Management Tool)**:
   - `Clusterctl` is a command-line tool used in conjunction with Cluster API, a Kubernetes project for managing the lifecycle of Kubernetes clusters. `Clusterctl` simplifies the process of creating, scaling, and managing Kubernetes clusters using the Cluster API.

1. **logcli (logs command-line interface Query Tool)**:
   - `logcli` is the command-line interface to Grafana Loki. It facilitates running LogQL queries against a Loki instance.
## Extented container includes basic tools and additional following tools :

1. **calico (Networking and Network Security for Containers)**:
   - `calico` is an open-source networking and network security solution designed for containers and container orchestration systems like Kubernetes. It provides features such as network segmentation, security policies, and integration with various cloud and on-premises environments.

1. **cilium (API-aware Network Security for Containers)**
   - `cilium` is a networking and security project that provides API-aware network security for containerized applications. It enhances container networking capabilities by offering features like load balancing, network visibility, and security enforcement based on application-layer protocols.

1. **cosign (Container Signing)**:
   - `cosign` is a tool for signing and verifying container images. It enables users to cryptographically sign container images to ensure their integrity and authenticity. This is crucial for security and trust in containerized environments.

1. **kubetail (Tail Logs from Multiple Pods)**
   - `kubetail` is a utility that enables tailing logs from multiple Kubernetes pods simultaneously. It simplifies log aggregation and monitoring by providing a convenient way to view logs from various pods in a single stream.

1. **Kubevirt (Kubernetes Extension for Virtualization)**:
   - `Kubevirt` extends Kubernetes to support virtualized workloads. It enables the deployment and management of virtual machines alongside containerized workloads in a Kubernetes cluster, providing a unified platform for both types of workloads.

1. **K9s (Terminal-based UI for Kubernetes)**
   - `K9s` is a terminal-based UI for interacting with Kubernetes clusters. It offers a convenient and efficient way to navigate and manage Kubernetes resources, providing insights into cluster state and allowing users to perform various operations through a text-based interface.

1. **Crustgather (Kubectl plugin)**
   - `[crust-gather](https://github.com/crust-gather/crust-gather)` is a kubectl plugin that collects all available resources across the cluster, including container logs, node kubelet logs. Resource collection is parallelized with exponential backoff retries and configurable timeouts. It is capable to filter collected resources based on regexes, include/excludes, namespaces, groups or kinds

## Deployement phase for basic container
```
docker run --rm registry.gitlab.com/sylva-projects/sylva-elements/container-images/sylva-toolbox:IMAGE_TAG | tar xz -C DESTINATION_FOLDER
```

## Deployement phase for extended container
```
docker run --rm registry.gitlab.com/sylva-projects/sylva-elements/container-images/sylva-toolbox/extented:IMAGE_TAG | tar xz -C DESTINATION_FOLDER
```
