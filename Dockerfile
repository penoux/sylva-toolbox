ARG BASE_IMAGE
ARG BASE_TAG

FROM alpine:3.20.3

ARG TARGETOS
ARG TARGETARCH

# OS of the binaries the image contains may be different from TARGETOS of the image
ARG BIN_TARGET_OS="linux"
ARG BASE_TAG

# renovate: datasource=gitlab-tags depName=sylva-projects/sylva-elements/sylvactl
ARG SYLVACTL_VERSION="v0.6.1"
# renovate: datasource=github-releases depName=kubernetes-sigs/kind VersionTemplate=v
ARG KIND_VERSION="0.24.0"
# renovate: datasource=github-tags depName=kubernetes/kubectl VersionTemplate=kubernetes-
ARG KUBECTL_VERSION="1.30.3"
# renovate: datasource=github-tags depName=kubernetes-sigs/kustomize VersionTemplate=kustomize/v
ARG KUSTOMIZE_VERSION="5.5.0"
# renovate: datasource=github-releases depName=helm/helm VersionTemplate=v
ARG HELM_VERSION="3.16.2"
# renovate: datasource=github-releases depName=fluxcd/flux2 VersionTemplate=v
ARG FLUX_VERSION="2.4.0"
# renovate: datasource=github-releases depName=mikefarah/yq VersionTemplate=v
ARG YQ_VERSION="4.44.5"
# renovate: datasource=github-releases depName=sigstore/cosign VersionTemplate=v
ARG COSIGN_VERSION="2.4.1"
# renovate: datasource=github-releases depName=kubernetes-sigs/cluster-api VersionTemplate=v
ARG CLUSTERCTL_VERSION="1.8.5"
# renovate: datasource=github-releases depName=grafana/loki VersionTemplate=v
ARG LOGCLI_VERSION="3.2.1"
# renovate: datasource=github-releases depName=crust-gather/crust-gather VersionTemplate=v
ARG CRUST_GATHER_VERSION="0.6.0"

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# hadolint ignore=DL3018, SC3037, DL3025, SC3036
RUN apk update \
    && apk --no-cache add bash wget gzip \
    && wget -q --show-progress --progress=bar:force https://gitlab.com/api/v4/projects/43501695/packages/generic/sylvactl/${SYLVACTL_VERSION}/sylvactl_${BIN_TARGET_OS}_${TARGETARCH} -O /usr/local/bin/sylvactl \
    && wget -q --show-progress --progress=bar:force https://kind.sigs.k8s.io/dl/v${KIND_VERSION}/kind-${BIN_TARGET_OS}-${TARGETARCH} -O /usr/local/bin/kind \
    && wget -q --show-progress --progress=bar:force https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/${BIN_TARGET_OS}/${TARGETARCH}/kubectl -O /usr/local/bin/kubectl \
    && wget -q --show-progress --progress=bar:force https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_${BIN_TARGET_OS}_${TARGETARCH}.tar.gz -O - | tar -xzO kustomize > /usr/local/bin/kustomize \
    && wget -q --show-progress --progress=bar:force https://get.helm.sh/helm-v${HELM_VERSION}-${BIN_TARGET_OS}-${TARGETARCH}.tar.gz -O - | tar -xzO ${BIN_TARGET_OS}-${TARGETARCH}/helm > /usr/local/bin/helm \
    && wget -q --show-progress --progress=bar:force https://github.com/fluxcd/flux2/releases/download/v${FLUX_VERSION}/flux_${FLUX_VERSION}_${BIN_TARGET_OS}_${TARGETARCH}.tar.gz -O - | tar -xzO flux > /usr/local/bin/flux \
    && wget -q --show-progress --progress=bar:force https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_${BIN_TARGET_OS}_${TARGETARCH} -O /usr/local/bin/yq \
    && wget -q --show-progress --progress=bar:force https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign-${BIN_TARGET_OS}-${TARGETARCH} -O /usr/local/bin/cosign \
    && wget -q --show-progress --progress=bar:force https://github.com/kubernetes-sigs/cluster-api/releases/download/v${CLUSTERCTL_VERSION}/clusterctl-${BIN_TARGET_OS}-${TARGETARCH} -O /usr/local/bin/clusterctl \
    && wget -q --show-progress --progress=bar:force https://github.com/grafana/loki/releases/download/v${LOGCLI_VERSION}/logcli-${BIN_TARGET_OS}-${TARGETARCH}.zip -O - | zcat > /usr/local/bin/logcli \
    && wget -q --show-progress --progress=bar:force https://github.com/crust-gather/crust-gather/releases/download/v${CRUST_GATHER_VERSION}/kubectl-crust-gather_${CRUST_GATHER_VERSION}_${BIN_TARGET_OS}_${TARGETARCH}.tar.gz -O - | tar -xzO kubectl-crust-gather > /usr/local/bin/crustgather \
    && chmod +x /usr/local/bin/sylvactl /usr/local/bin/kind /usr/local/bin/kubectl /usr/local/bin/kustomize /usr/local/bin/helm /usr/local/bin/flux /usr/local/bin/yq /usr/local/bin/cosign /usr/local/bin/clusterctl /usr/local/bin/logcli /usr/local/bin/crustgather \
    && if [ "${BIN_TARGET_OS}/${TARGETARCH}" == "linux/amd64" ]; then \
        kind version \
        && kubectl version --client \
        && kustomize version \
        && helm version \
        && flux -v \
        && yq --version \
        && cosign version \
        && clusterctl version \
        && logcli --version \
        && crustgather -V; \
    fi \
    && echo -e "sylva-toolbox:${BASE_TAG}\n\nSYLVACTL_VERSION:${SYLVACTL_VERSION}\nKIND_VERSION:${KIND_VERSION}\nKUBECTL_VERSION:${KUBECTL_VERSION}\nKUSTOMIZE_VERSION:${KUSTOMIZE_VERSION}\nHELM_VERSION:${HELM_VERSION}\nFLUX_VERSION:${FLUX_VERSION}\nYQ_VERSION:${YQ_VERSION}\nCOSIGN_VERSION:${COSIGN_VERSION}\nCLUSTERCTL_VERSION:${CLUSTERCTL_VERSION}\nLOGCLI_VERSION:${LOGCLI_VERSION}\nCRUST_GATHER_VERSION:${CRUST_GATHER_VERSION}\n" > /sylva-toolbox-version \
    && cat sylva-toolbox-version

COPY env /usr/local/bin
# hadolint ignore=DL3025
CMD tar -czf - -C /usr/local/bin $(ls /usr/local/bin) /sylva-toolbox-version
