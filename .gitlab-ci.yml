---
default:
  tags:
    - gitlab-org-docker

variables:
  DOCKER_BUILD_ARGS: "--build-arg BASE_IMAGE=${BASE_IMAGE} \
    --build-arg BASE_TAG=${BUILD_TAG} \
    --build-arg BIN_TARGET_OS=${BIN_TARGET_OS}"
  TARGETPLATFORM: "linux/amd64,linux/arm64"
  OS_IMAGE_REGISTRY: "${CI_REGISTRY_IMAGE}"
  IMAGE_REGISTRY: ${OS_IMAGE_REGISTRY}
  SYLVA_CORE_BRANCH:
    value: main
    description: "Sylva-core branch to use for cross-repo pipeline"

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        BASE_IMAGE: ${OS_IMAGE_REGISTRY}
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_PIPELINE_SOURCE == 'web'
      variables:
        BASE_IMAGE: ${OS_IMAGE_REGISTRY}/snapshot

stages:
  - lint
  - build
  - test
  - extended-image

include:
  - project: "to-be-continuous/gitleaks"
    ref: "2.6.1"
    file: "templates/gitlab-ci-gitleaks.yml"
  - project: 'renovate-bot/renovate-runner'
    ref: v19.28.1
    file: '/templates/renovate-config-validator.gitlab-ci.yml'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        changes:
          paths:
            - .gitlab-ci.yml
            - renovate.json
  - project: 'sylva-projects/sylva-elements/renovate'
    ref: 1.0.0
    file: '/templates/renovate-dry-run.gitlab-ci.yml'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        changes:
          paths:
            - renovate.json
  - project: 'sylva-projects/sylva-elements/ci-tooling/ci-templates'
    ref: 1.0.31
    file:
      - 'templates/docker-build.yml'
      - 'templates/release-notes.yml'

.docker-base:
  parallel:
    matrix:
      - BIN_TARGET_OS: "linux"
        OS_IMAGE_REGISTRY: "${CI_REGISTRY_IMAGE}"
        IMAGE_REGISTRY_EXTENDED: "${OS_IMAGE_REGISTRY}/extended"
      - BIN_TARGET_OS: "darwin"
        OS_IMAGE_REGISTRY: "${CI_REGISTRY_IMAGE}/${BIN_TARGET_OS}"
        # Gitlab limits additional names to the end of a container image name, up to two levels deep
        IMAGE_REGISTRY_EXTENDED: "${OS_IMAGE_REGISTRY}-extended"

docker-hadolint:
  parallel: null

container_scanning-extended:
  stage: extended-image
  dependencies: []
  needs:
    - docker-build-extended
  extends: container_scanning
  variables:
    DOCKERFILE: Dockerfile-extended
    IMAGE_REGISTRY: ${IMAGE_REGISTRY_EXTENDED}

docker-hadolint-extended:
  stage: extended-image
  dependencies: []
  extends: docker-hadolint
  variables:
    DOCKER_FILE: Dockerfile-extended
    IMAGE_REGISTRY: ${IMAGE_REGISTRY_EXTENDED}

docker-build-extended:
  stage: extended-image
  dependencies: []
  needs:
    - docker-build
    - docker-hadolint-extended
  extends: docker-build
  variables:
    DOCKER_FILE: Dockerfile-extended
    IMAGE_REGISTRY: ${IMAGE_REGISTRY_EXTENDED}

# Cross repo pipeline is manual as only maintainers of Sylva-core can run it
sylva-core-cross-project:
  stage: test
  variables:
    SYLVA_TOOLBOX_VERSION: "0.0.0-git-$CI_COMMIT_SHORT_SHA"
    SYLVA_TOOLBOX_IMAGE: container-images/sylva-toolbox/snapshot
  trigger:
    project: sylva-projects/sylva-core
    branch: $SYLVA_CORE_BRANCH
    strategy: depend
  rules:
    - if: $CI_MERGE_REQUEST_LABELS =~ /renovate/
      when: manual
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: manual
    - if: $CI_PIPELINE_SOURCE == 'web'
      when: manual
  allow_failure: true
